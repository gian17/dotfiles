config.load_autoconfig(False)
config.bind('cs', 'config-source')
config.bind('ce', 'config-edit')
# c.editor.command = ["st", "-e", "nvim", "-c", "startinsert", "{file}"]
c.editor.command = ["st", "-e", "nvim", "{file}"]
c.content.blocking.method = "both"
c.downloads.location.prompt = False
# config.source('redirectors.py')

# Theme
config.source('themes/gruvbox.py')
# css = '~/.config/qutebrowser/themes/gruvbox-all-sites.css'
# config.bind(',n', f'config-cycle content.user_stylesheets {css} ""')

c.colors.webpage.darkmode.enabled = True
# c.colors.webpage.darkmode.algorithm = 'lightness-hsl'
c.colors.webpage.darkmode.algorithm = 'brightness-rgb'
c.colors.webpage.darkmode.contrast = -.022
c.colors.webpage.darkmode.threshold.text = 150
c.colors.webpage.darkmode.threshold.background = 205
c.colors.webpage.darkmode.policy.images = 'always'
c.colors.webpage.darkmode.grayscale.all = True
c.colors.webpage.darkmode.grayscale.images = 0.35
c.colors.webpage.darkmode.grayscale.images = 0.90
# c.window.transparent = True
# c.tabs.show = 'switching'
# default size
c.fonts.web.size.default = 26
# Engines
c.url.default_page = "https://search.brave.com"
c.url.start_pages = "https://search.brave.com"
c.url.searchengines = {
        "DEFAULT": "https://search.brave.com/search?q={}",
        "aw": "https://wiki.archlinux.org/?search={}",
        "g": "https://www.google.it/search?q={}"
        }

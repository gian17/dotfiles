;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Gianluca Tadori"
      user-mail-address "gianluca.tadori@mailo.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;   :config
;;   (setq mixed-pitch-set-heigth t)
;;   (set-face-attribute 'variable-pitch nil :height 1.3))

;; (use-package! mixed-pitch
;;   :hook
;;   ;; If you want it in all text modes:
;;   (text-mode . mixed-pitch-mode)
;;   :config
;;   (set-face-attribute 'variable-pitch nil :height 1.3))
(setq doom-font (font-spec :family "Noto Sans Mono" :size 14)
      doom-variable-pitch-font (font-spec :family "Libertinus Serif" :size 25))
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox)

;; Set transparency settings
(add-to-list 'default-frame-alist '(alpha . 80))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")
(setq org-roam-directory "~/org/")

 ;; (let* ((variable-tuple
 ;;          (cond ((x-list-fonts "ETBembo")         '(:font "ETBembo"))
 ;;                ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
 ;;                ((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
 ;;                ((x-list-fonts "Verdana")         '(:font "Verdana"))
 ;;                ((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
 ;;                (nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro."))))
 ;;         (base-font-color     (face-foreground 'default nil 'default))
 ;;         (headline           `(:inherit default :weight bold :foreground ,base-font-color)))

    ;; (custom-theme-set-faces!
    ;;  `(org-level-8 ((t (,@headline ,@variable-tuple))))
    ;;  `(org-level-7 ((t (,@headline ,@variable-tuple))))
    ;;  `(org-level-6 ((t (,@headline ,@variable-tuple))))
    ;;  `(org-level-5 ((t (,@headline ,@variable-tuple))))
    ;;  `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
    ;;  `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
    ;;  `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
    ;;  `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
    ;;  `(org-document-title ((t (,@headline ,@variable-tuple :height 2.0 :underline nil))))))
    ;;
    ;;

  ;; '(org-drawer :inherit org-document-info-keyword :foreground unspecified :height .8)
  ;; '(org-meta-line :inherit org-document-info-keyword :foreground unspecified :height .9)
    ;;(custom-set-faces!
    ;;
;; (custom-set-faces!
;;   '(org-level-1 :inherit default :weight bold :foreground face-foreground 'default nil 'default :height 1.75 :font "ETBembo")
;;   '(org-level-2 :height 1.5)
;;   '(org-level-3 :height 1.25)
;;   '(org-level-4 :height 1.1))

(custom-set-faces!
  '(org-document-title :inherit default :weight bold :height 2.0 :underline nil)
  '(org-level-1 :inherit default :weight bold :height 1.75)
  '(org-level-2 :inherit default :weight bold :height 1.5)
  '(org-level-3 :inherit default :weight bold :height 1.25)
  '(org-level-4 :inherit default :weight bold :height 1.1))
(add-hook 'org-mode-hook 'writeroom-mode)
;; (use-package! mixed-pitch
;;   :hook (org-mode . mixed-pitch-mode)
;;   :config
;;   (setq mixed-pitch-set-heigth t)
;;   (set-face-attribute 'variable-pitch nil :height 1.3))
;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Settings related to fonts
;; doom-font: standard monospace font (used in code)
;; doom-variable-pitch-font': variable font, used in writing
;; doom-big-font: used in doom-big-font-mode
;; font-lock-comment-face: for comments
;; font-lock-keyword-face: for keywords like for and if
(defun zz/org-download-paste-clipboard (&optional use-default-filename)
  (interactive "P")
  (require 'org-download)
  (let ((file
         (if (not use-default-filename)
             (read-string (format "Filename [%s]: "
                                  org-download-screenshot-basename)
                          nil nil org-download-screenshot-basename)
           nil)))
    (org-download-clipboard file)))

(after! org
  (setq org-download-method 'directory)
  (setq org-download-image-dir "images")
  (setq org-download-heading-lvl nil)
  (setq org-download-timestamp "%Y%m%d-%H%M%S_")
  (setq org-image-actual-width 300)
  (map! :map org-mode-map
        "C-c l a y" #'zz/org-download-paste-clipboard
        "C-M-y" #'zz/org-download-paste-clipboard))
